
package uz.pdp.appteamprojectolx.entity;


import lombok.*;
import uz.pdp.appteamprojectolx.entity.enums.RoleEnum;
import uz.pdp.appteamprojectolx.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
//    @Enumerated(EnumType.STRING)
    private String role;

    @ManyToMany
    List<Permission> permissions;

    public Role(String role, List<Permission> permissions) {
        this.role = role;
        this.permissions = permissions;
    }
}
