package uz.pdp.appteamprojectolx.entity;

import lombok.*;
import uz.pdp.appteamprojectolx.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
//@AllArgsConstructor
@Getter
@Setter
@Entity(name = "tariffs")
public class Tariff extends AbsEntity {

    private String name;

    private int days;

    public Tariff(String name, int days) {
        this.name = name;
        this.days = days;
    }
}