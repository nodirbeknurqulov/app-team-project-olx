package uz.pdp.appteamprojectolx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appteamprojectolx.entity.template.AbsEntity;

import javax.persistence.*;

//@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "attachment_content")
public class AttachmentContent extends AbsEntity {

    byte[] data;

    @OneToOne(cascade = CascadeType.MERGE)
    private Attachment attachment;

    public AttachmentContent(byte[] data, Attachment attachment) {
        this.data = data;
        this.attachment = attachment;
    }
}