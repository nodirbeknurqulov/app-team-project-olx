package uz.pdp.appteamprojectolx.entity;

import lombok.*;
import uz.pdp.appteamprojectolx.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

// Nurkulov Nodirbek 4/12/2022  11:37 AM
@NoArgsConstructor
//@AllArgsConstructor
@Getter
@Setter
@Entity(name = "advertisements")
public class Advertisement extends AbsEntity {

    @Column(nullable = false)
    private String shortDescription;

    @ManyToOne
    private Category category;

    @OneToMany
    private List<Attachment> attachmentList;

    private String longDescription;

    private Double initialPrice;

    @OneToOne
    private Tariff tariff;

    private boolean isNew=true;

    @ManyToOne
    private User user;

    private boolean isActive;

    public Advertisement(String shortDescription, Category category, List<Attachment> attachmentList, String longDescription, Double initialPrice, Tariff tariff, boolean isNew, User user, boolean isActive) {
        this.shortDescription = shortDescription;
        this.category = category;
        this.attachmentList = attachmentList;
        this.longDescription = longDescription;
        this.initialPrice = initialPrice;
        this.tariff = tariff;
        this.isNew = isNew;
        this.user = user;
        this.isActive = isActive;
    }
}
