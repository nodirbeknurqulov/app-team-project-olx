package uz.pdp.appteamprojectolx.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "regions")
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "region", cascade = CascadeType.MERGE)
    private List<District> districtList;

    public Region(String name) {
        this.name = name;
//        this.districtList = districtList;
    }
}

