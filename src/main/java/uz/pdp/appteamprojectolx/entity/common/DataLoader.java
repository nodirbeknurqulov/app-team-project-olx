package uz.pdp.appteamprojectolx.entity.common;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.pdp.appteamprojectolx.entity.*;
import uz.pdp.appteamprojectolx.entity.enums.RoleEnum;
import uz.pdp.appteamprojectolx.entity.repository.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    final TariffRepository tariffRepository;
    final RoleRepository roleRepository;
    final RegionRepository regionRepository;
    final AttachmentRepository attachmentRepository;
    final AdvertisementRepository advertisementRepository;

    public DataLoader(RoleRepository roleRepository, RegionRepository regionRepository, AttachmentRepository attachmentRepository, AdvertisementRepository advertisementRepository, CategoryRepository categoryRepository,
                      DistrictRepository districtRepository, UserRepository userRepository,TariffRepository tariffRepository) {
        this.roleRepository = roleRepository;
        this.regionRepository = regionRepository;
        this.attachmentRepository = attachmentRepository;
        this.advertisementRepository = advertisementRepository;
        this.categoryRepository = categoryRepository;
        this.districtRepository = districtRepository;
        this.userRepository = userRepository;
        this.tariffRepository = tariffRepository;
    }

    final CategoryRepository categoryRepository;
    final DistrictRepository districtRepository;
    final UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {


        Role role_admin = roleRepository.save(new Role("ROLE_ADMIN", null));
        Role role_user = roleRepository.save(new Role("ROLE_USER", null));

        Set<Role> roles = new HashSet<>();

        roles.add(role_admin);
        roles.add(role_user);

//
//        userRepository.save(new User
//                ("Jony", "jony@mail.com", "1",
//                        "998998888888", 450000.0, roles));

        User jony = new User
                ("Jony", "jony@mail.com", "1",
                        "998998888888", 450000.0, roles);
        userRepository.save(jony);

        Tariff start = new Tariff("Start",7);
        tariffRepository.save(start);

        Region tashkent = new Region("Tashkent");
        regionRepository.save(tashkent);
//        regionRepository.save(new Region("Tashkent"));

        districtRepository.save(new District("Shayhantahur", tashkent));

        Attachment iphone12 = new Attachment("Phone Iphone 12", 23, "imagePng");
        attachmentRepository.save(iphone12);

        List<Attachment> attachments=new ArrayList<>();
        attachments.add(iphone12);


        Category electronics = new Category("Electronics", true, null);
        categoryRepository.save(electronics);

//    categoryRepository.save(new Category("Smartphones", true, 1));



        advertisementRepository.save(new Advertisement("Amazing phone", electronics,
               attachments, "Blablablablablablablabla", 650.0,
                start, true, jony, true));
    }


}
