package uz.pdp.appteamprojectolx.entity.enums;

public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_USER
}
