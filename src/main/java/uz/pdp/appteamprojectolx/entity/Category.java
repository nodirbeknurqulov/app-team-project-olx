
package uz.pdp.appteamprojectolx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appteamprojectolx.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//@AllArgsConstructor
@Data
@NoArgsConstructor
@Entity(name = "categories")
public class Category extends AbsEntity {

    private String name;

    private boolean isEnable;

    private Long parentId;

    public Category(String name, boolean isEnable, Long parentId) {
        this.name = name;
        this.isEnable = isEnable;
        this.parentId = parentId;
    }
}
