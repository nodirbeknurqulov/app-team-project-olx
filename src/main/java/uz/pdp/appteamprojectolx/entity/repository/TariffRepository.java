package uz.pdp.appteamprojectolx.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appteamprojectolx.entity.Tariff;

public interface TariffRepository extends JpaRepository<Tariff,Integer> {

}

