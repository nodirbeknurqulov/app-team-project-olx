package uz.pdp.appteamprojectolx.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appteamprojectolx.entity.Region;

public interface RegionRepository extends JpaRepository<Region, Integer> {

}
