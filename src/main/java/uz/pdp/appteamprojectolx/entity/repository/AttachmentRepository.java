package uz.pdp.appteamprojectolx.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appteamprojectolx.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {


}
