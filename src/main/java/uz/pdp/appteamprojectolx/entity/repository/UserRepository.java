package uz.pdp.appteamprojectolx.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appteamprojectolx.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
