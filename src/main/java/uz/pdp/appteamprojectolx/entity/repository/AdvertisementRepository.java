package uz.pdp.appteamprojectolx.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appteamprojectolx.entity.Advertisement;

public interface AdvertisementRepository extends JpaRepository<Advertisement, Integer> {

}
