
package uz.pdp.appteamprojectolx.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Address;
import uz.pdp.appteamprojectolx.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

//@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "users")
public class User extends AbsEntity {

    @Column(nullable = false)
    private String fullName;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(unique = true)
    private String phoneNumber;

    private Double balance;

    @ManyToMany(cascade = CascadeType.MERGE)
    private Set<Role> role;

    public User(String fullName, String email, String password, String phoneNumber, Double balance, Set<Role> role) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.balance = balance;
        this.role = role;
    }
}

