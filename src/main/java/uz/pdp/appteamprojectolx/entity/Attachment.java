package uz.pdp.appteamprojectolx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import uz.pdp.appteamprojectolx.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
//@AllArgsConstructor
@Data
@Entity(name = "attachments")
public class Attachment extends AbsEntity {

    private String name;

    private long size;

    private String contentType;

    public Attachment(String name, long size, String contentType) {
        this.name = name;
        this.size = size;
        this.contentType = contentType;
    }
}
