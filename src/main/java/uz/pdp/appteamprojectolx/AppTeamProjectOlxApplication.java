package uz.pdp.appteamprojectolx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppTeamProjectOlxApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppTeamProjectOlxApplication.class, args);
    }
}
